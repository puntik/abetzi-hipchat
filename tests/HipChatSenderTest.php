<?php

namespace Abetzi\HipChat;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class HipChatSenderTest extends TestCase
{

	const OK_CODE          = 200;
	const NOT_FOUND_CODE   = 404;
	const UNAUTHORIZE_CODE = 401;

	public function testSuccessfulSending()
	{
		$this->markTestIncomplete('Not implemented yet');
	}

	public function testCannotSendAMessageWhenRoomIsNotFilled()
	{
		$response = new Response(self::NOT_FOUND_CODE);
		$client   = $this->createClientMock($response);

		$this->expectException(\Exception::class);
		$this->expectExceptionCode(self::NOT_FOUND_CODE);

		$hipChatSender = new HipChatSender($client, 'no-auth');
		$hipChatSender
			->setColor(HipChatSender::COLOR_INFO)
			->sendMessage('Sending message should raise an exception.');
	}

	public function testCannotSendWithWrongAuthenticationKey()
	{
		$this->expectException(\Exception::class);
		$this->expectExceptionCode(self::UNAUTHORIZE_CODE);

		$response = new Response(self::UNAUTHORIZE_CODE);
		$client   = $this->createClientMock($response);

		$hipChatSender = new HipChatSender($client, 'no-auth');
		$hipChatSender
			->setColor(HipChatSender::COLOR_INFO)
			->setRoom('my-room')
			->sendMessage('Hello world');
	}

	public function testCannotSendToUnknownRoom()
	{
		$response = new Response(self::NOT_FOUND_CODE);
		$client   = $this->createClientMock($response);

		$this->expectException(\Exception::class);
		$this->expectExceptionCode(self::NOT_FOUND_CODE);

		$hipChatSender = new HipChatSender($client, 'no-auth');
		$hipChatSender
			->setColor(HipChatSender::COLOR_INFO)
			->setRoom('my-room')
			->sendMessage('Hello world');
	}

	private function createClientMock(Response $response): Client
	{
		$mock    = new MockHandler([ $response, ]);
		$handler = HandlerStack::create($mock);

		return new Client([ 'handler' => $handler ]);
	}
}
