# Simplest hipchat client

The simplest hipchat client for sending notifications. :) Works with hipchat api v.2.0.

## Installation: 
```shell
composer require abetzi/hipchat
```
## Usage:
```php
<?php
use Abetzi\HipChat\HipChatSender;

$url = 'your hipchat endpoint';
$authToken = 'your hipchat authentification key';

$httpClient = new \GuzzleHttp\Client(
	[
		'base_url' => $url,
	]
);
$hipchat = new HipChatSender($httpClient, $authToken); // use DI and config files instead
$hipchat
	->setRoom('roomName')                              // required, where to send a message
	->setColor(HipChatSender::COLOR_SUCCESS)           // optional, color of message, default is gray
	->sendMessage('Hello world');
```

Color is named by Bootstrap CSS framework by its meaning.

### Example of nette neon config file

```yaml
services:
	# setting of guzzle client to connect hipchat endpoint
	-
		class: GuzzleHttp\Client
		arguments:
			- base_uri:  %hipchat.baseUri%
			
	# settings of hipchat sender
	- Abetzi\HipChat\HipChatSender(..., %hipchat.authToken%)
``` 
