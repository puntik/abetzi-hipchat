<?php declare(strict_types = 1);

namespace Abetzi\HipChat;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class HipChatSender
{

	const COLOR_INFO    = 'gray';
	const COLOR_SUCCESS = 'green';
	const COLOR_DANGER  = 'red';
	const COLOR_WARNING = 'yellow';
	const COLOR_NOTICE  = 'purple';

	/** @var Client */
	private $client;

	/** @var string */
	private $messageFormat;

	/** @var string */
	private $color;

	/** @var string */
	private $authToken;

	/** @var string */
	private $room;

	public function __construct(
		Client $client,
		string $authToken
	) {
		$this->client        = $client;
		$this->authToken     = $authToken;
		$this->messageFormat = 'html';
	}

	public function setRoom(string $room): self
	{
		$this->room = $room;

		return $this;
	}

	public function setText(): self
	{
		$this->messageFormat = 'text';

		return $this;
	}

	public function setColor(string $color): self
	{
		$this->color = $color;

		return $this;
	}

	public function sendMessage(string $message, bool $notify = false)
	{
		if ($this->room === null) {
			throw new \Exception('No room found, please use setRoom.', 404);
		}

		$uri = sprintf('room/%s/notification?auth_token=%s', $this->room, $this->authToken);

		$json = [
			'color'          => $this->color,
			'message'        => $message,
			'notify'         => $notify,
			'message_format' => $this->messageFormat,
		];

		try {
			$this->client->post(
				$uri,
				[
					'json' => $json,
				]
			);

			return true;
		} catch (ClientException $ce) {
			$responseCode = $ce->getCode();

			if ($responseCode === 401) {
				$message = 'Wrong authToken. Please, check your settings of authorization.';
			} elseif ($responseCode === 404) {
				$message = sprintf('Room \'%s\' not found. Please, check if this room exists.', $this->room);
			} else {
				$message = 'Something is wrong.';
			}

			throw new \Exception(
				sprintf($message),
				$ce->getCode(),
				$ce
			);
		} catch (\Throwable $e) {
			throw $e;
		}
	}
}
